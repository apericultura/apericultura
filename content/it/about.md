---
title: "Chi siamo"
description: ""

menus: main
draft: false
---

Siamo un gruppo di studenti dell'Università di Trieste, desiderosi di confrontarsi e discutere alcune tematiche *calde* nei nostri rispettivi ambiti di studio (prevalentemente STEM).

## Cos'è un aperitivo culturale?

Gli ingredienti sono pochi:
- una sala accogliente
- un aperitivo leggero offerto dalla casa
- qualche persona eventualmente interessata all'argomento (o alle libagioni)
- un tema di cui chiacchierare scambiando idee ed opinioni.

Il tema viene proposto da uno dei presenti, che aprirà il dibattito con una riflessione iniziale abbastanza elaborata da impostare il tema ma comunque il più concisa possibile, per lasciare spazio alla discussione libera delle idee di ognuno (e al libero riempimento dei calici).

## Tutto qui?

Da ogni aperitivo emergono parecchi spunti di riflessione originali. Non sappiamo che farcene, se non annotarli su queste pagine e consultarli quando la mente lo richiede!
