---
date: 2024-03-22
description: "Sintesi del terzo aperitivo culturale"
title: "Aperitivo 3 - Cos'è la fisica?"

draft: false
show_reading_time: true
---


Gli argomenti per l’aperitivo di questo mese sono ispirati da alcuni
risultati riportati dall’articolo [*A century of physics*](https://doi.org/10.1038/nphys3494) proposto da
Giovanni. Nel paper gli autori cercano di definire *cos’è la fisica* studiando il risultato della produzione scientifica dei fisici, ovvero gli 
articoli pubblicati.

In particolare, l'articolo prova a suddividere il corpus di articoli
scientifici pubblicati in tre categorie: fisica "pura", fisica 
interdisciplinare e non-fisica. All'interno delle varie categorie 
è possibile effettuare suddivisioni più raffinate, alle quali non 
è stato dato grande peso durante la discussione. In termini molto semplici,
la ripartizione degli articoli nelle tre categorie è stata effettuata
consultando le citazioni di ogni articolo, e controllando a quale categoria esse stesse appartenessero.

Le prime questioni discusse durante l'aperitivo hanno preso spunto 
da alcuni risultati mostrati nell'articolo; in particolare, 
si fa riferimento alla crescita nel tempo del numero di articoli 
interdisciplinari pubblicati e al fatto che alcuni dei settori della 
letteratura *fisica* tendano a essere di nicchia, con un'elevata 
autoreferenzialità e con la tendenda a non interfacciarsi 
con lavori esterni al loro ambito.

Detto questo ci si è posti le seguenti domande:

-   ad oggi si percepisce una sempre maggiore necessità di specializzazione: è veramente necessaria?

-   è necessario aumentare l'interconnessione tra settori? Se si, è fattibile, e con che mezzi?

La necessità di definirsi 
-------------------------

Fin da subito ci siamo trovati d’accordo sull’importanza di una
formazione ampia, evitando la tendenza a specializzarsi eccessivamente 
fin dagli ultimi anni della formazione universitaria; inoltre, gran parte
dei presenti ritiene che la troppa specializzazione abbia dei risvolti
negativi. 

Ciò detto, è sorta la problematica dell'attualità e della fattibilità
dell'ampliamento delle conoscenze del fisico, non solo dal punto di 
vista tecnico; a questo punto però, è emersa la necessità di fare 
chiarezza sulla definizione di *fisico* e di *scienziato*, facendo leva 
sulla necessita dell’individuo di definirsi, di affermare chi/cosa è 
e chi/cosa non è.
Il timore condiviso dai presenti è che l'affermare, *e.g.* 
di essere un fisico, possa in qualche modo giustificare la
un disinteresse nei confronti delle branche del sapere lontane
dal proprio ambito di specializzazione; ciò renderebbe difficoltoso 
qualunque tentativo di interdisciplinarietà.

In seguito, il discorso si è orientato sulla figura dello "scienziato" 
inteso come persona di cultura non esclusivamente *tecnica*.
Grazie a un giro di domande che ha coinvolto tutti i presenti, è emerso  
che in pochi ritengono opportuno definirsi *scienziato* 
a causa di una visione idealizzata del termine e del ruolo a esso
associato. 
Da qui, si è cercato di definire meglio la figura dello scienziato a 
partire dalle caratteristiche che la contraddistinguono, *e.g.* 
la curiosità e l'interesse per ogni branca del sapere. 
Un utile esercizio mentale adottato allo scopo è il seguente: 

>Immaginiamo un eremita che vive isolato dal
>mondo e che, osservando il ciclo delle piogge, riesce a comprenderne il
>funzionamento, a sfruttarlo e dunque a a migliorare il proprio 
raccolto. Costui è uno scienziato?" 


L'esempio aiuta a mettere in luce l'indole dello scienziato in termini
assoluti, indipendentemente dal grado di istruzione raggiunto; egli è
dotato idealmente di una curiosità innata che alimenta il desiderio 
di espandere le proprie conoscenze. Quest'ultimo punto ha fatto emergere
la differenza fra uno *scienziato* in senso stretto (come *persona di 
scienza*), e un *tecnico della scienza*, non necessariamente dotato del
desiderio di ampliare le proprie conoscenze per il puro gusto di conoscere.

Un ulteriore problema dell'esperimento mentale proposto sta nel considerare
erroneamente l'istruzione come variabile trascurabile ai fini della 
discussione - essa gioca un ruolo essenziale nella formazione dello 
scienziato.

L’accademia forma scienziati?
-----------------------------

La tematica dell'istruzione ha condotto la discussione verso la domanda
> Il sistema scolastico e universitario forma scienziati o tecnici?

La maggior parte dei presenti ha affermato che il sistema scolastico 
forma principalmente tecnici. E' stato messo in luce il fatto che 
i corsi universitari non comunicano a sufficienza tra loro, conducendo a 
una considerevole perdita di tempo e di risorse. In parte, si tratta di
un male necessario, essendo il professore universitario medio
estremamente specializzato (e, si spera, interessato) nel proprio ambito,
e perciò ben più capace nel proporre argomenti a esso affini rispetto a 
temi al di fuori di esso. Inoltre, non tutti gli ottimi ricercatori sono
anche ottimi didatti; non tutti i professori sono abili divulgatori, 
affascinati dagli innumerevoli collegamenti della propria disciplina con 
gli altri insegnamenti del corso di laurea.

Tirando le somme, l'opinione dominante dei presenti conferisce alle 
università il ruolo di ampliare gli orizzonti degli studenti, lasciando 
al mondo del lavoro o a ulteriori percorsi specialistici il compito di 
educare alla specializzazione. 
Dunque, la specializzazione non è un male, ma è inevitabile e 
necessaria per poter raggiungere le frontiere del sapere; ciononostante,
essa non deve costringere *colui che fa scienza* a essere prima tecnico, 
e solo successivamente persona di cultura a tutto tondo. In breve, 
> la scienza non è puramente tecnica, ma è anche cultura.

Conclusioni
-----------

Facendo la media delle opinioni espresse da ciascuno durante la serata, 
è possibile affermare la presenza di una sempre maggiore specializzazione
del ricercatore, fatto necessario e inevitabile, dovuto al numero crescente
di persone di scienza e un sempre più lontano orizzonte del sapere da 
raggiungere. Al contempo, è necessario un adeguato livello di 
interdisciplinarietà per poter favorire un dialogo fra le persone di 
scienza e oltrepassare con più efficacia la frontiera della conoscenza. 
La mancanza di un'innata curiosità non costituisce di per sè un limite 
per lo scienziato *in fieri*; le istituzioni che contribuiscono alla 
formazione della *persona di scienza* devono fornire gli strumenti per 
per poter accrescere la propria *cultura scientifica* al di là delle 
semplici competenze tecniche.

Partecipanti:
-------------
Emma, Marcello, Iacopo, Ermes, Enrico, Chiara, Camilla, Alberta, Francesco
Federico, Giovanni, Alessia, Enrico.
