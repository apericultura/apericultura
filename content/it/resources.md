---
title: "Fonti"
description: ""

menus: main
draft: false
---

Ogni aperitivo parte da una fonte, che sia un articolo, un libro o un podcast. Di seguito, ecco le fonti degli ultimi aperitivi:

- Il declino della cultura scientifica (26 gennaio 2024): [Gli scienziati italiani non vengono ascoltati perchè non hanno una voce - Roberto Defez](https://open.spotify.com/episode/5MWezlZlm2JiuIWf7OwrdE?si=J3cvbfCaRnGZrFZtMsns7g&nd=1&dlsi=f18021e006c54557) 
- Il problema della demarcazione (29 febbraio 2024): [The Demise of the Demarcation Problem - Larry Laudan](https://link.springer.com/chapter/10.1007/978-94-009-7055-7_6) 
- A century of physics (22 marzo 2024): [A century of physics - Sinatra et al.](https://doi.org/10.1038/nphys3494)

