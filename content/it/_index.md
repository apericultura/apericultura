---
title: "Aperitivi Culturali"

description: "Un tema, un aperitivo leggero, qualche ora di chiacchiere."
# 1. To ensure Netlify triggers a build on our exampleSite instance, we need to change a file in the exampleSite directory.
theme_version: '2.8.2'
# cascade:
#   featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

Un tema, un aperitivo leggero, qualche ora di chiacchiere.
