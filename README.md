# apericultura

A static website (using Hugo) containing the proceedings of the Aperitivi Culturali meetings.

## Quick start

To edit the site contents, you should:

1. get access: contact the authors of the site providing your Gitlab username
2. clone the repository
```
git clone git@gitlab.com:apericultura/apericultura.git
cd apericultura
``` 
### Add a post

To add a post, you should know some basic Markdown. Navigate to `contents/it/post/` and create a new Markdown file with the following header
```
---
date: YYYY-MM-DD
title: "YOUR TITLE HERE"
description: "YOUR DESCRIPTION HERE"
draft: false
---
```
and add your post below. Save it, and push your commit.

> You can hide a post from view using `draft: true`.

## Commits

Please follow [this](https://cbea.ms/git-commit/) guideline to write commit messages! Readability is important.

## Author

- website: [Iacopo Ricci](https://www.iricci.frama.io)
- contents: the Aperitivi Culturali group
